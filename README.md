# meson-testoptions

Try out all combinations of meson build options.
It respects the option types 'feature' and 'boolean'.

## Usage

Go to a meson build directory and execute this script.
It will then try out all possible meson build options.

Further help can be found via `./meson-testoptions --help`.

Attention: This script reconfigures the build directory multiple times and does not reset the old configuration.

## License

SPDX-License-Identifier: GPL-3.0-or-later

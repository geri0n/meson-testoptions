#!/usr/bin/env python
#
# Copyright (C) 2021 Gerion Entrup
#
# SPDX-License-Identifier: GPL-3.0-or-later
# See LICENSE for more information.
#
"""Try out all combinations of meson build options."""

import subprocess
import argparse
import sys
import logging
import json
import itertools

meson = ''
mesonerror = "Current directory is not a meson build directory."


def get_buildoptions():
    o = subprocess.run([meson, 'introspect', '--buildoptions'],
                       capture_output=True)
    if mesonerror.encode('UTF-8') in o.stdout:
        logging.error(o.stdout.decode('UTF-8'))
        raise RuntimeError(mesonerror)
    return(json.loads(o.stdout))


def is_a_feature(option):
    # Meson introspect has no indication whether an option is a feature or a
    # combo. Therefore, we assume that a feature is a combo option with the
    # choices 'enabled', 'disabled' and 'auto'.
    return (option['type'] == 'combo' and
            len(option['choices']) == 3 and
            set(('enabled', 'disabled', 'auto')) == set(option['choices']))


def filter_options(options):
    options = [x for x in options if x['section'] == 'user']
    return [x for x in options if x['type'] == 'boolean' or is_a_feature(x)]


def config_project(combination, options):
    cmd = [meson, 'configure']
    for c, o in zip(combination, options):
        if o['type'] == 'combo':
            value = {True: 'enabled', False: 'disabled'}[c]
        else:
            value = str(c)
        cmd.append(f"-D{o['name']}={value}")
    logging.info(f"Configure with: {' '.join(cmd)}")
    subprocess.run(cmd, check=True)


def clean_project():
    subprocess.run([meson, 'compile', '--clean'], check=True)


def build_project():
    subprocess.run([meson, 'compile'], check=True)


def build_and_test_project():
    subprocess.run([meson, 'test'], check=True)


def main():
    parser = argparse.ArgumentParser(prog=sys.argv[0],
                                     description=sys.modules[__name__].__doc__)
    parser.add_argument('--meson', default='meson',
                        help='path of meson program')
    parser.add_argument('--quiet', '-q', default=False, action='store_true',
                        help='Set loglevel to warn.')
    parser.add_argument('--verbose', '-v', default=False, action='store_true',
                        help='Set loglevel to debug.')
    parser.add_argument('--clean', '-c', default=False, action='store_true',
                        help='Clean project after each reconfigure.')
    parser.add_argument('--test', '-t', default=False, action='store_true',
                        help='Also run tests after building.')
    parser.add_argument('--allyes', '-y', default=False, action='store_true',
                        help='Test only with all options activated.')
    args = parser.parse_args()

    level = logging.INFO
    if args.quiet:
        level = logging.WARN
    if args.verbose:
        level = logging.DEBUG

    logging.basicConfig(level=level)

    global meson
    meson = args.meson

    options = get_buildoptions()
    options = filter_options(options)

    # import pprint
    # pprint.pprint(options)

    if args.allyes:
        combinations = 1
        iterator = iter([len(options)*[True]])
    else:
        combinations = 2**len(options)
        iterator = itertools.product([False, True], repeat=len(options))
    for i, c in enumerate(iterator):
        logging.info(f"Combination {i+1}/{combinations}")
        config_project(c, options)
        if args.clean:
            clean_project()
        if args.test:
            build_and_test_project()
        else:
            build_project()


if __name__ == '__main__':
    main()
